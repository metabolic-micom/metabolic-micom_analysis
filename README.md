# Analyzing simulation output

Scripts and notebooks were run on either one of the 2 systems:
- Personal laptop
- Peregrine HPC
both of which are in Linux environment.

## Personal laptop
### Operating system
NAME ="Manjaro Linux"
ID=manjaro
ID_LIKE=arch
BUILD_ID=rolling

### Python

- Conda environment: "py36"
- Version: 3.6.12 :: Anaconda, Inc.
- Packages:
  - *pandas-1.1.3*: 
  - *numpy-1.19.2*
  - *matplotlib-3.3.2*
  - *seaborn-0.11.1*

### R

- Conda environment: "micom"
- Version: 3.5.1 (2018-07-02) -- "Feather Spray"
- Packages:
  - *reshape-0.8.8*; *reshape2-1.4.4*
  - *ggplot2-3.2.1*
  - *gridExtra-2.3*
  - *plot3D-1.3*
  - *pheatmap-1.0.12*

## Peregrine HPC

### Operating system

NAME="CentOS Linux" 
VERSION="7 (Core)" 
ID="centos" 
ID_LIKE="rhel fedora" 
VERSION_ID="7"

### Python

- From module: "Python/3.6.6-foss-2018b"
- Version: 3.6.6
- Packages:
  - *pandas-1.1.3*: `pip install --user pandas==1.1.3`
  - *numpy-1.19.5*
  - *matplotlib-3.3.4*
  - *seaborn-0.11.1*
  - *scikit-learn-0.24.1*
  - *scipy-1.5.4*

### R

- From module: "R/3.6.1-foss-2018a"
- Version: 3.6.1 (2019-07-05) -- "Action of the Toes"
- Packages:
  - *reshape-0.8.8*; *reshape2-1.4.3*
  - *ggplot2-3.2.0*
  - *gridExtra-2.3*
  - *plot3D-1.1.1*
  - *pheatmap-1.0.12*
