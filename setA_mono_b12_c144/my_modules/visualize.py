import pandas as pd
import numpy as np

from my_modules import compute

from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns

sns.set_style("whitegrid")

font = {'family': 'sans-serif', 'serif' : 'Helvetica',
        'size'   : 25}
plt.rc('font', **font)

my_quali_colormaps = ['colorblind', 'Dark2']

#final_OD_palette = {'ON': '#3b8080', 'OFF': '#a1a9c5'}
#final_OD_palette = {'ON': '#5f9292', 'OFF': '#a1a9c5'}
final_OD_palette = {'ON': '#ffb219', 'OFF': '#a1a9c5'}

std_final_OD_palette = {'Batch culture | Plasticity = ON': '#b57e11', 'Batch culture | Plasticity = OFF': '#5c6895', 
                        'Continuous culture | Plasticity = ON': '#ffb219', 'Continuous culture | Plasticity = OFF': '#a1a9c5'}

def generate_time_ticks(max_time):
    if max_time <= 2.0:
        return np.arange(0.0, max_time+1.0, 0.5)
    elif max_time <= 6.0:
        return np.arange(0.0, max_time+1.0, 1.0)
    elif max_time <= 12.0:
        return np.arange(0.0, max_time+1.0, 2.0)
    elif max_time <= 24.0:
        return np.arange(0.0, max_time+1.0, 6.0)
    elif max_time <= 72.0:
        return np.arange(0.0, max_time+1.0, 12.0)
    else:
        return np.arange(0.0, max_time+1.0, 24.0)

#################################################
# Plotting bacteria density (OD) over time

def plot_OD_over_time(storage, simID, replicates, culture_type, media, style='overlay', save_fig=True, fig_dir='./', fig_name=None):
    
    # Monitor running process & validate arguments
    status = 0
    
    if culture_type not in ['batch', 'continuous']:
        raise ValueError('Name of culture type should only be either \'batch\' or \'continuous\'')
    
    if style not in ['overlay', 'facetgrid']:
        raise ValueError('Name of style should only be either \'overlay\' or \'facetgrid\'')
    
    #### Prepare data frame
    merged_data = pd.DataFrame()
    for rep in replicates:
        for m in media:

            data_dir = storage + rep + '/v2.10.0_sim_' + simID + '/experiments/' + culture_type + '_mono-culture/'  + m + '/'
            data_file = data_dir + 'species_trajectory.tsv'

            df = pd.read_csv(data_file, sep="\t", usecols=['time', 'od'])
            df['media'] = m
            df['replicate'] = rep

            merged_data = pd.concat([merged_data, df])

        merged_data.reset_index(drop=True)
    
    #### Plotting    
    if style == 'overlay':
        fig, ax = plt.subplots(figsize=(12, 7))

        kwargs={'linewidth':1}

        p = sns.lineplot(data=merged_data, x='time', y='od', hue='media', 
                         estimator='mean', ci='sd', err_kws={'alpha': 0.2}, # line represents mean across replicates, and shaded area represents standard deviation
                         palette=sns.color_palette(my_quali_colormaps[0], len(media)), ax=ax, **kwargs)
        
        # Put the legend out of the figure
        leg = ax.legend(bbox_to_anchor=(1.05, 1), loc=2, title="Media", framealpha=0.5)
        # Change width of lines in legend
        for line in leg.get_lines():
            line.set_linewidth(5)

        # Add title
        title = simID + " | " + culture_type 
        ax.set_title(title)
        ax.set(xlabel='Time (h)', ylabel='OD')
    
    elif style == 'facetgrid':
        max_time = merged_data['time'].max()

        kwargs={'linewidth':4, 'palette': sns.color_palette(my_quali_colormaps[1], len(replicates))}
        
        col_wrap = 3 if len(media) > 2 else len(media)

        grid = sns.FacetGrid(data=merged_data,  col='media', col_wrap=col_wrap, sharex=True, sharey=False, legend_out=True, height=5, aspect=1.5)

        grid.map_dataframe(sns.lineplot, data=merged_data, x='time', y='od', hue='replicate', **kwargs)

        # General settings
        grid.fig.suptitle(simID + " | " + culture_type, fontsize=30, y=1.05)
        grid.set_xlabels("Time (h)")
        grid.set_ylabels("OD")

        # x-axis ticks
        grid.set(xticks=generate_time_ticks(max_time))
        
        legnd_ncol = 1 if len(replicates) <= 5 else 2
        legnd = grid.axes[-1].legend(loc='upper left', bbox_to_anchor=(1.2, 1.0), title='Replicate', ncol=legnd_ncol)
        # Change width of lines in legend
        for line in legnd.get_lines():
            line.set_linewidth(5)
        
    if save_fig: # Save plot
        fig_name = fig_dir + simID + "." + culture_type + ".OD_over_time." + style + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close()
        
        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
    return status

#################################################
# Plotting bacteria growth rate over time or over density (OD)

def const_line(flowRate=0.1, fluxNGAM=0.1, *args, **kwargs):
#     plt.axhline(y=0.0, color='black', linestyle='-')
    plt.axhline(y=0.0, color='black', linestyle='--')
    plt.axhline(y=flowRate, color='blue', linestyle='--')
    plt.axhline(y=-fluxNGAM, color='red', linestyle='--') 

def plot_growthRate_over_time(storage, simID, replicates, culture_type, media, save_fig=True, fig_dir='./', fig_name=None):
    
    # Monitor running process & validate arguments
    status = 0
    
    if culture_type not in ['batch', 'continuous']:
        raise ValueError('Name of culture type should only be either \'batch\' or \'continuous\'')
    
    #### Get hardcoded parameters
    sim_dir = storage + 'v2.10.0_sim_' + simID + '/'
    hardcoded_param = pd.read_csv(sim_dir + 'hardcoded_parameters.tsv', sep='\t')
    flowRate = hardcoded_param['flowRate'][0]
    fluxNGAM = hardcoded_param['fluxNGAM'][0]
    
    #### Prepare data frame
    merged_data = pd.DataFrame()
    for rep in replicates:
        for m in media:

            data_dir = storage + rep + '/v2.10.0_sim_' + simID + '/experiments/' + culture_type + '_mono-culture/'  + m + '/'
            data_file = data_dir + 'species_trajectory.tsv'

            df = pd.read_csv(data_file, sep="\t", usecols=['time', 'r'])
            df['media'] = m
            df['replicate'] = rep

            merged_data = pd.concat([merged_data, df])

        merged_data.reset_index(drop=True)
    
    #### Plotting    
    max_time = merged_data['time'].max()

    kwargs={'linewidth':4, 'palette': sns.color_palette(my_quali_colormaps[1], len(replicates))}

    col_wrap = 3 if len(media) > 2 else len(media)

    grid = sns.FacetGrid(data=merged_data,  col='media', col_wrap=col_wrap, sharex=True, sharey=False, legend_out=True, height=5, aspect=1.5)

    grid.map_dataframe(sns.lineplot, data=merged_data, x='time', y='r', hue='replicate', **kwargs)

    grid.map(const_line, flowRate=flowRate, fluxNGAM=fluxNGAM)
    
     # General settings
    grid.fig.suptitle(simID + " | " + culture_type, fontsize=30, y=1.05)
    grid.set_xlabels("Time (h)")
    grid.set_ylabels("Growth rate")

    # x-axis ticks
    grid.set(xticks=generate_time_ticks(max_time))

    legnd_ncol = 1 if len(replicates) <= 5 else 2
    legnd = grid.axes[-1].legend(loc='upper left', bbox_to_anchor=(1.2, 1.0), title='Replicate', ncol=legnd_ncol)
    # Change width of lines in legend
    for line in legnd.get_lines():
        line.set_linewidth(5)
        
    if save_fig: # Save plot
        fig_name = fig_dir + simID + "." + culture_type + ".growthRate_over_time.svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
    return status

# Plotting bacteria growth rate over density (OD) (single replicate)
# Just works for single experiment result: data from 1 single media in 1 single culture type of 1 single replicate
from matplotlib.collections import LineCollection
def gradient_line(data, x, y, gradient, ymin, ymax, ax, cmap='hot_r', lw=2): # Sourced from matplotlib documentation
    x_ = np.array(data[x])
    y_ = np.array(data[y])
    grad = np.array(data[gradient])[1:]
    
    # Create a set of line segments so that we can color them individually
    # This creates the points as a N x 1 x 2 array so that we can stack points
    # together easily to get the segments. The segments array for line collection
    # needs to be (numlines) x (points per line) x 2 (for x and y)
    points = np.array([x_, y_]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    
    # Create a continuous norm to map from data points to colors
    norm = plt.Normalize(0.0, 1.0)
    lc = LineCollection(segments, cmap=cmap, norm=norm)
    
    # Set the values used for colormapping
    lc.set_array(grad)
    lc.set_linewidth(lw)
    line = ax.add_collection(lc)
    
    ax.set_xlim(0.0, x_.max()+5.0)
    ax.set_ylim(-0.2, ymax)
    
    return line

def plot_growthRate_over_od_single(storage, replicate, simID, culture_type, medium, max_time='final', save_fig=True, fig_dir='./', fig_name=None):
    
    # Monitor running process & validate arguments
    status = 0
    
    if culture_type not in ['batch', 'continuous']:
        raise ValueError('Name of culture type should only be either \'batch\' or \'continuous\'')
    
    #### Get hardcoded parameters
    sim_dir = storage + replicate + '/v2.10.0_sim_' + simID + '/'
    hardcoded_param = pd.read_csv(sim_dir + 'hardcoded_parameters.tsv', sep='\t')
    flowRate = hardcoded_param['flowRate'][0]
    fluxNGAM = hardcoded_param['fluxNGAM'][0]
    
    ### Get mode
    mode = simID.rsplit('_', 1)[1]
    
    #### Prepare data frame
    data_dir = storage + replicate + '/v2.10.0_sim_' + simID + '/experiments/' + culture_type + '_mono-culture/'  + medium + '/'
    data_file = data_dir + 'species_trajectory.tsv'
    
    cols = ['time', 'od', 'r']
    if mode == 'plastic':
        for i in range(20):
            cols.append('r_' + str(i))
            cols.append('p_' + str(i))
        
    df = pd.read_csv(data_file, sep="\t", usecols=cols)
    df = df.loc[df['time'] <= max_time, :] if max_time != 'final' else df

    #### Plotting
    if mode == 'plastic':
        fig, ax = plt.subplots(figsize=(10, 7))
        for i in range(20):
            grad_line = gradient_line(data=df, x='od', y='r_' + str(i), gradient='p_' + str(i), 
                                      ymin=-0.1, ymax=df['r'].max()+1.0, 
                                      cmap='YlOrBr', ax=ax)

            if i==19:
                fig.colorbar(grad_line, ax=ax)
    else:
        fig, ax = plt.subplots(figsize=(8, 7))
        
    sns.lineplot(data=df, x='od', y='r', sort=False, color='black', ax=ax)

    ax.axhline(y=flowRate, color='#3d4594', linestyle='--')
    ax.axhline(y=-fluxNGAM, color='#e9044e', linestyle='--') 

    ax.set_xlabel('OD')
    ax.set_ylabel('Growth rate')

    if save_fig: # Save plot
        fig_name = fig_dir + simID + "." + culture_type + "." + medium + ".growthRate_over_od.svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
    return status

#################################################
# Plotting OD at final time point
# Bar plot for each species

def plot_final_OD(storage, speciesID, culture_type, media, fig_dir="./"):
    # Monitor running process & validate arguments
    status = 0
    
    new_labels = {'plastic': 'ON', 'sfm': 'OFF'}
    
    if isinstance(culture_type, str):
        if culture_type not in ['batch', 'continuous']:
            raise ValueError('Name of culture type should only be either \'batch\' or \'continuous\'')
        
        #### Prepare data frame
        final_OD_file = storage + "final_point." + culture_type + "_mono-culture.od.tsv"

        final_OD = pd.read_csv(final_OD_file, sep="\t")
        final_OD = final_OD.loc[final_OD['species_ID']==speciesID, :].reset_index(drop=True)
        final_OD_melted = pd.melt(final_OD, id_vars=['rep_name', 'species_ID', 'mode'], value_vars=media, 
                                  var_name='media', value_name='final_OD')
        final_OD_melted = final_OD_melted.replace({"mode": new_labels}).rename(columns = {'mode':'plasticity'})
        
        #### Plotting
        fig, ax = plt.subplots(figsize=(15, 7))

        sns.barplot(data=final_OD_melted, x='media', y='final_OD', hue='plasticity', 
                    ci='sd', palette=final_OD_palette, saturation=1.0, ax=ax)

        xticklabels = ax.get_xticklabels()
        ax.set_xticklabels(xticklabels, rotation = 45, ha="right", 
                           fontdict={'fontsize': 24})

        # Put the legend out of the figure
        ax.legend(bbox_to_anchor=(1.0, 1.0), loc=2, title="Plasticity", framealpha=0.5)

        # Add title
        title = simID + " | " + culture_type.capitalize() + " culture" 
        ax.set_title(title)
        ax.set(xlabel='Media', ylabel='OD at final time point')
        
        #### Figure filename
        fig_name = fig_dir + speciesID + ".final_OD." + culture_type + ".svg"

        
    elif isinstance(culture_type, list):
        if set(culture_type) != {'batch', 'continuous'}:
            raise ValueError('`culture_type` should only be a list of \'batch\' and \'continuous\'')
        
        #### Prepare data frame
        merged_data = pd.DataFrame()
        for cult in culture_type:
            final_OD_file = storage + "final_point." + cult + "_mono-culture.od.tsv"

            final_OD = pd.read_csv(final_OD_file, sep="\t")
            final_OD = final_OD.loc[final_OD['species_ID']==speciesID, :].reset_index(drop=True)

            final_OD_melted = pd.melt(final_OD, id_vars=['rep_name', 'species_ID', 'mode'], value_vars=media, 
                                      var_name='media', value_name='final_OD')
            final_OD_melted = final_OD_melted.replace({"mode": new_labels}).rename(columns = {'mode':'plasticity'})
            final_OD_melted['culture_type'] = cult

            merged_data = pd.concat([merged_data, final_OD_melted])
        merged_data.reset_index(drop=True)

        #### Plotting
        kwargs={'ci': 'sd', 'palette': final_OD_palette, 'saturation': 1.0}

        grid = sns.FacetGrid(data=merged_data,  row='culture_type', sharex=True, sharey=True, legend_out=True, height=5, aspect=3)

        grid.map_dataframe(sns.barplot, data=merged_data, x='media', y='final_OD', hue='plasticity', **kwargs)

        # General settings
        grid.fig.subplots_adjust(top=0.85)
        grid.fig.suptitle(speciesID, fontsize=35)
        grid.set_xlabels("Media")
        grid.set_ylabels("OD at final time point")
        grid.fig.subplots_adjust(wspace=0.2, hspace=0.4)
        # grid.fig.set_size_inches(15,17)

        for i in range(len(culture_type)):
            grid.axes[i,0].set_title(culture_type[i].capitalize() + ' culture')

        legnd = grid.axes[0,0].legend(loc='upper right', bbox_to_anchor=(1.0, 1.5), title='Plasticity')

        xticklabels = grid.axes[1,0].get_xticklabels()
        grid.set_xticklabels(xticklabels, rotation = 45, ha="right", fontdict={'fontsize': 24})
        
        #### Figure filename
        fig_name = fig_dir + speciesID + ".final_OD.facetgrid.svg"
    
    else:
        raise ValueError('`culture_type` should only be either a string of either \'batch\' or \'continuous\', or a list of those two values')

    # Save plot
    plt.savefig(fig_name, transparent=True, bbox_inches='tight')
    plt.close()
    
    status = fig_name + ": Plotted & Saved."
    return status

#################################################
# Plotting standard deviation in final OD across replicates
# Bar plot for each species

def plot_std_final_OD(storage, speciesID, media, fig_dir="./"):
    # Monitor running process & validate arguments
    status = 0
    
    culture_type = ['batch', 'continuous']

    new_labels = {'batch_plastic': 'Batch culture | Plasticity = ON', 'batch_sfm': 'Batch culture | Plasticity = OFF', 
                  'continuous_plastic': 'Continuous culture | Plasticity = ON', 'continuous_sfm': 'Continuous culture | Plasticity = OFF'}
    
    #### Prepare data frame
    merged_data = pd.DataFrame()
    for cult in culture_type:
        final_OD_file = storage + "final_point." + cult + "_mono-culture.od.tsv"

        final_OD = pd.read_csv(final_OD_file, sep="\t")
        final_OD = final_OD.loc[final_OD['species_ID']==speciesID, :].reset_index(drop=True)

        final_OD_melted = pd.melt(final_OD, id_vars=['rep_name', 'species_ID', 'mode'], value_vars=media, 
                                  var_name='media', value_name='final_OD')
        final_OD_melted['label'] = cult + '_' + final_OD_melted['mode']
        final_OD_melted = final_OD_melted.replace({'label': new_labels})

        merged_data = pd.concat([merged_data, final_OD_melted])
    merged_data.reset_index(drop=True)
    
    ## Compute standard deviation
    std_df = merged_data.groupby(['media', 'label']).std().reset_index().rename(columns = {'final_OD':'std_final_OD'})
    
    ### Plotting
    fig, ax = plt.subplots(figsize=(15, 7))

    sns.barplot(data=std_df, x='media', y='std_final_OD', hue='label', hue_order=new_labels.values(),
                ci=None, palette=std_final_OD_palette, saturation=1.0, ax=ax)

    xticklabels = ax.get_xticklabels()
    ax.set_xticklabels(xticklabels, rotation = 45, ha="right", 
                       fontdict={'fontsize': 24})

    # Put the legend out of the figure
    ax.legend(bbox_to_anchor=(1.0, 1.0), loc=2, title="Culture type | State of plasticity", framealpha=0.5)

    # Add title
    title = speciesID 
    ax.set_title(title)
    ax.set(xlabel='Media', ylabel=r'\sigma_{density}')

    # Save plot
    fig_name = fig_dir + speciesID + ".std_final_OD" + ".svg"
    plt.savefig(fig_name, transparent=True, bbox_inches='tight')
    plt.close()
    
    status = fig_name + ": Plotted & Saved."
    return status

#################################################
# Plotting distribution of final OD across replicates

def plot_OD_dist(plastic, sfm, palette, ax):
    if plastic.std() > 0.1:
        sns.violinplot(data=plastic, color=palette['ON'], saturation=1.0, ax=ax)
        ax.collections[0].set_alpha(0.3)

    ax.axhline(y=plastic.mean(), color=palette['ON'], linewidth=4.0)

    sns.swarmplot(data=plastic, color=palette['ON'], edgecolor="white", size=10, ax=ax)

    ax.axhline(y=sfm, color=palette['OFF'], linewidth=4.0)

    ax.set_xticklabels('')
    ax.yaxis.set_major_formatter(plt.FormatStrFormatter('%.2f'))
    return ax

# Plot for single species in single medium
def plot_OD_dist_single(storage, culture_type, species_ID, medium, fig_dir='./'):
    # Monitor running process & validate arguments
    status = 0
    
    if culture_type not in ['batch', 'continuous']:
        raise ValueError('Name of culture type should only be either \'batch\' or \'continuous\'')
        
    # Prepare dataframe
    data = pd.read_csv(storage + "final_point." + culture_type + "_mono-culture.od.tsv", sep="\t")

    plastic = data.loc[(data['species_ID']==species_ID) & (data['mode']=='plastic'), medium].to_numpy()
    sfm = data.loc[(data['species_ID']==species_ID) & (data['mode']=='sfm'), medium].mean()
    
    # Plotting
    fig, ax = plt.subplots(figsize=(5,6))
    ax = plot_OD_dist(plastic, sfm, final_OD_palette, ax)
    ax.set_xlabel(medium)
    ax.set_ylabel(r'$\rho$')
    ax.set_title(species_ID)
    
    # Save plot
    fig_name = fig_dir + "OD_dist." + culture_type + '_' + species_ID + '_' + medium + ".svg"
    plt.savefig(fig_name, transparent=True, bbox_inches='tight')
    plt.close()
    
    status = fig_name + ": Plotted & Saved."
    return status

# Plot over multiple species & multiple media
def plot_OD_dist_grid(storage, culture_type, ordered_species_list, ordered_media_list, fig_dir='./'):
    # Monitor running process & validate arguments
    status = 0
    
    if culture_type not in ['batch', 'continuous']:
        raise ValueError('Name of culture type should only be either \'batch\' or \'continuous\'')
        
    # Prepare dataframe
    data = pd.read_csv(storage + "final_point." + culture_type + "_mono-culture.od.tsv", sep="\t")

    data = pd.melt(data, id_vars=['species_ID', 'rep_name', 'mode'], value_vars=ordered_media_list, 
                   var_name='media', value_name='final_OD')
    
    plastic_df = data.loc[data['mode']=='plastic', :]
    sfm_df = data.loc[data['mode']=='sfm', :]
    
    # Plotting
    fig = plt.figure(figsize=(10+5*len(ordered_media_list), 6*len(ordered_species_list)))
    fig.suptitle(culture_type.capitalize() + ' culture', x=0.5, y=0.89)

    outer = gridspec.GridSpec(nrows=len(ordered_species_list), ncols=len(ordered_media_list), figure=fig, wspace=0.5, hspace=0.2)

    for i in range(len(ordered_species_list)):

        sp = ordered_species_list[i]

        for j in range(len(ordered_media_list)):

            medium = ordered_media_list[j]
            plastic = plastic_df.loc[(plastic_df['species_ID'] == sp) & (plastic_df['media']==medium), 'final_OD'].to_numpy()
            sfm = sfm_df.loc[(sfm_df['species_ID'] == sp) & (sfm_df['media']==medium), 'final_OD'].mean()

            plot_ax = fig.add_subplot(outer[i, j])

            plot_ax = plot_OD_dist(plastic, sfm, final_OD_palette, plot_ax)

            if j==0:
                plot_ax.set_ylabel(sp)
            elif j==len(ordered_media_list)-1:
                plot_ax.yaxis.set_label_position("right")
                plot_ax.set_ylabel(sp)

            if i==0:
                plot_ax.xaxis.set_label_position("top")
                plot_ax.set_xlabel(medium)
            elif i==len(ordered_species_list)-1:
                plot_ax.set_xlabel(medium)
    
    # Save plot
    fig_name = fig_dir + "OD_dist." + culture_type + ".svg"
    plt.savefig(fig_name, transparent=True, bbox_inches='tight')
    plt.close()
    
    status = fig_name + ": Plotted & Saved."
    return status

#################################################
# Plotting flux dynamic
def plot_flux_dynamic(storage, replicates, speciesID, media, max_time=None, normalize=True, plot_biomass=False, save_fig=True, fig_dir='./'):
    # Monitor running process & validate arguments
    status = 0
    
    culture_type_list = ['batch', 'continuous']

    flux_dyn = pd.DataFrame()

    for rep in replicates:
        for cult in culture_type_list:
            for m in media:
                data_df = compute.flux_dynamics(storage, rep, speciesID, cult, m, tmax=max_time, normalize=normalize, plot_biomass=plot_biomass)
                data_df['media'] = m
                data_df['culture_type'] = cult
                data_df['replicate'] = rep

                flux_dyn = pd.concat([flux_dyn, data_df])
    flux_dyn.reset_index(drop=True)
    
    if max_time is None:
        max_time = flux_dyn['t_end'].max()
    
    # Plotting
    kwargs={'linewidth':2, 'palette': sns.color_palette(my_quali_colormaps[1], len(replicates))}

    grid = sns.FacetGrid(data=flux_dyn,  row='media', col='culture_type', sharex=True, sharey='row', 
                         margin_titles=True, legend_out=True, height=7, aspect=1.2)

    grid.map_dataframe(sns.lineplot, data=flux_dyn, x='t_end', y='rate_of_change', hue='replicate', **kwargs)

    # General settings
    grid.fig.suptitle(speciesID + "_plastic", fontsize=30, y=1.005)
    grid.set_xlabels("Time (h)")
    grid.set_ylabels("Rate of flux change")

    # x-axis ticks
    grid.set(xticks=generate_time_ticks(max_time))

    legnd_ncol = 1 if len(replicates) <= 5 else 2
    legnd = grid.axes[0,1].legend(loc='upper right', bbox_to_anchor=(1.0, 1.0), title='Replicate', ncol=legnd_ncol)
    # Change width of lines in legend
    for line in legnd.get_lines():
        line.set_linewidth(5)

    if save_fig: # Save plot
        fig_name = fig_dir + speciesID + ".flux_dynamic" + ".svg"
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1

    return status

#################################################
# Plotting metabolite consumption & production dynamic

def metaprod_normalization(row):
    production_denominator = row[row >= 0].sum()
    consumption_denominator = row[row < 0].sum()
    
    normalized = []
    for r in row:
        if r >= 0.0:
            n = r/production_denominator if (production_denominator != 0.0) else r
        else:
            n = r/consumption_denominator*(-1) if (consumption_denominator != 0.0) else r
        normalized.append(n)
    return normalized

def plot_metaprod_dynamic(storage, replicate, simID, media, max_time=None, normalize=True, save_fig=True, fig_dir='./', fig_name=None):
    # Monitor running process & validate arguments
    status = 0
    
    culture_type_list = ['batch', 'continuous']

    metaprod_dyn = pd.DataFrame()

    i=-1
    for cult in culture_type_list:
        i+=1
        for m in media:
            data_dir = storage + replicate + '/v2.10.0_sim_' + simID + '/experiments/' + cult + '_mono-culture/'  + m + '/'
            data_file = data_dir + 'species_metabolite_production.tsv'

            data_df = pd.read_csv(data_file, sep="\t").drop('species_name', axis=1).iloc[:, :-1]

            if max_time is not None:
                data_df = data_df.loc[data_df['time'] <= max_time[i]] if max_time[i] != 'final' else data_df

            if normalize:
                normalized = data_df.iloc[:, 1:].apply(metaprod_normalization, axis=1)
                normalized_df = pd.DataFrame(normalized.to_list())
                normalized_df.columns = data_df.iloc[:, 1:].columns
                normalized_df['time'] = data_df['time']

                normalized_df['media'] = m
                normalized_df['culture_type'] = cult

                metaprod_dyn = pd.concat([metaprod_dyn, normalized_df])

            else:
                data_df['media'] = m
                data_df['culture_type'] = cult

                metaprod_dyn = pd.concat([metaprod_dyn, data_df])

    # Remove metabolites with zero production/consumption
    metaprod_dyn = metaprod_dyn.loc[:, metaprod_dyn.sum()!=0.0]

    metaprod_dyn.reset_index(drop=True)    

    metabolites = list(metaprod_dyn.drop(['time', 'media', 'culture_type'], axis=1).columns)
    
    # Plotting
    fig, ax = plt.subplots(ncols=2, nrows=len(media), sharex=False, sharey=True, figsize=(20, len(media)*10))

    # Plot for each media
    for i in range(len(media)):

        selected_metaprod_dyn = metaprod_dyn.loc[metaprod_dyn['media'] == media[i], :]

        for j in range(len(culture_type_list)):

            plot_data = selected_metaprod_dyn.loc[selected_metaprod_dyn['culture_type'] == culture_type_list[j], :]
            plot_data = pd.melt(plot_data, id_vars='time', value_vars=metabolites, var_name='metabolite', value_name='proportion')
            plot_data['class'] = plot_data['proportion'].apply(lambda x: 'produced' if x>0.0 else 'consumed')
            plot_data['proportion'] = plot_data['proportion'].abs()

            plot_max_time = plot_data['time'].max()
            plot_xticks = generate_time_ticks(plot_max_time)

            kwargs={'linewidth':1}
            
            if len(media) > 1:
                plot_ax = ax[i, j]
            else:
                plot_ax = ax[j]

            if (i < (len(media)-1)) | (j == 1):
                g = sns.lineplot(data=plot_data, x='time', y='proportion', hue='metabolite', style='class', ax=plot_ax, legend=False, **kwargs)
            else:
                g = sns.lineplot(data=plot_data, x='time', y='proportion', hue='metabolite', style='class', ax=plot_ax, legend='auto', **kwargs)
                legnd = plot_ax.legend(loc='upper left', bbox_to_anchor=(0.0, -0.1), ncol=2)
                # Change width of lines in legend
                for line in legnd.get_lines():
                    line.set_linewidth(5)

            g.set(xticks=plot_xticks, ylim=(0.0, 1.0), 
                  title = media[i] + ' | ' + culture_type_list[j], 
                  xlabel='Time (h)', ylabel='Consumption/Production proportion')

    if save_fig: # Save plot
        fig_name = fig_dir + speciesID + ".metaprod_dynamic" + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1

    return status