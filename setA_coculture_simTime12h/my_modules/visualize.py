import pandas as pd
import numpy as np

import gc # Garbage Collector for releasing unreferenced memory

# from my_modules import compute

from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns

sns.set_style("whitegrid")

font = {'family': 'sans-serif', 'serif' : 'Helvetica',
        'size'   : 25}
plt.rc('font', **font)

my_quali_colormaps = ['colorblind', 'Dark2']

def generate_time_ticks(max_time):
    if max_time <= 2.0:
        return np.arange(0.0, max_time+1.0, 0.5)
    elif max_time <= 6.0:
        return np.arange(0.0, max_time+1.0, 1.0)
    elif max_time <= 12.0:
        return np.arange(0.0, max_time+1.0, 2.0)
    elif max_time <= 24.0:
        return np.arange(0.0, max_time+1.0, 6.0)
    elif max_time <= 72.0:
        return np.arange(0.0, max_time+1.0, 12.0)
    else:
        return np.arange(0.0, max_time+1.0, 24.0)

#################################################
# Plotting bacteria density (OD) over time

def plot_OD_over_time(storage, simID, replicates, media, save_fig=True, fig_dir="./", fig_name=None):

    # Monitor running process & validate arguments
    status = 0
    
    check_culture_type = set(r.rsplit('_')[1] for r in replicates)

    if len(check_culture_type) > 1:
        raise ValueError('Function only works for one type of culture, either batch or continuous')

    # Metadata of species names & ecotypes
    meta_file = storage + 'selected_species_setA.tsv'
    meta = pd.read_csv(meta_file, sep="\t")
    
    # Prepare dataframe
    merged_data = pd.DataFrame()

    for r in replicates:
        rep = r.rsplit('_')[0]

        for m in media:
            data_dir = storage + rep + '/v2.10.0_sim_' + simID + '/experiments/' + list(check_culture_type)[0] + '_coculture/'  + m + '/'
            data_file = data_dir + 'species_trajectory.tsv'

            df = pd.read_csv(data_file, sep="\t", usecols=['species_name', 'time', 'od'])
            df['media'] = m
            df['replicate'] = rep

            merged_data = pd.concat([merged_data, df])

    members = meta.loc[meta['Species_name'].isin(df['species_name'].unique()), :]
#     print(members)

    merged_data.reset_index(drop=True)

    # Plotting
    fig = plt.figure(figsize=(14, 15*len(media)))

    outer = gridspec.GridSpec(nrows=len(media), ncols=1, wspace=0.2, hspace=0.2)

    for i in range(len(media)):
        inner = gridspec.GridSpecFromSubplotSpec(nrows=members.shape[0], ncols=1,
                                                 subplot_spec=outer[i], wspace=0.15, hspace=0.15)

        for j in range(members.shape[0]):

            plot_data = merged_data.loc[(merged_data['species_name'] == members.iloc[j, 3]) & (merged_data['media'] == media[i]), 
                                        ['time', 'od', 'replicate']]

            plot_ax = plt.Subplot(fig, inner[j])

            kwargs={'linewidth':3}
            g = sns.lineplot(data = plot_data, x='time', y='od', hue='replicate', ax=plot_ax, 
                             palette = sns.color_palette(my_quali_colormaps[1], len(replicates)), 
                             **kwargs)
            title = members.iloc[j, 1] + " (" + members.iloc[j, 2] + ")" 
            plot_ax.set(title=title, ylabel='OD')

            if j == members.shape[0]-1:
                plot_max_time = plot_data['time'].max()
                plot_ax.set(xlabel='Time (h)', xticks=generate_time_ticks(plot_max_time))
            else:
                plot_ax.set(xlabel='', xticklabels=[])

            if j == 0:

                plot_ax.text(0.0, 1.1, s=media[i], ha='center', va='center', transform=plot_ax.transAxes, 
                             fontweight='bold')
                lgnd = plot_ax.legend(loc='lower right', bbox_to_anchor=(1.0, 1.0), title='Replicate')
                # Change width of lines in legend
                for line in lgnd.get_lines():
                    line.set_linewidth(5)

            else:
                plot_ax.get_legend().remove()

            fig.add_subplot(plot_ax)
    
    # Save figure
    if save_fig:
        fig_name = fig_dir + simID + "." + list(check_culture_type)[0] + ".OD_over_time" + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close("all")
        plt.clf()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
        
    # Freeing memory usage
    del df, merged_data, plot_data
    del fig
    gc.collect()
    
    return status