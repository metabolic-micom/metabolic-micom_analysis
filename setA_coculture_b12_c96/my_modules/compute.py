import pandas as pd
import numpy as np

from scipy import linalg

def stack_co_replicates(data_dir, culture_type, medium, feature='od', plastic=False, members=None):
    # feature = 'od' or 'p_alive'
    # members = list of ecotype IDs
    mode = 'plastic' if plastic else 'sfm'
    data_file = data_dir + 'final_point.' + culture_type + '_coculture.' + mode + '.' + feature + '.tsv'

    essen = ['rep_name', 'media', 'ecotype', 'ecotype_ID', 'species_ID', 'species_name']
    cols = None if members is None else essen + members
    df = pd.read_csv(data_file, sep='\t', usecols=cols) # note that the order of species will be kept as in the source data file
    
    members = members if members is not None else df['ecotype_ID'].unique()
    
    data = df.loc[(df['media'] == medium) & (df['ecotype_ID'].isin(members)), :]
    replicates = data['rep_name'].unique()

    n_replicates = len(replicates)
    n_members = data.shape[1] - len(essen)
    data_mat = np.zeros((n_replicates, n_members, n_members))

    for i in range(n_replicates):
        data_layer = data.loc[data['rep_name'] == replicates[i], :]
        layer = data_layer.drop(essen, axis=1).to_numpy(dtype='float')
        data_mat[i, :, :] = layer

        if i == 0:
            meta_dict = {'ecotype': data_layer['ecotype'].to_list(), 
                         'ecotype_ID': data_layer['ecotype_ID'].to_list(), 
                         'species_ID': data_layer['species_ID'].to_list(), 
                         'species_name': data_layer['species_name'].to_list()}
    return data_mat, meta_dict

def competitive_exclusion_stack(data_dir, culture_type, medium, feature='od', min_survived=1.0e-2, plastic=False, members=None):
    
    stack_rep, meta = stack_co_replicates(data_dir, culture_type, medium, feature=feature, plastic=plastic, members=members)

    ce_stack = stack_rep.copy()
    ce_stack[stack_rep < min_survived] = -1.0 # excluded member
    ce_stack[stack_rep >= min_survived] = 0.0 # non-excluded member
    ce_stack[np.transpose(stack_rep, (0, 2, 1)) < min_survived] = 1.0 # outcomptetitor
    
    return ce_stack, meta

def stack_co_media(data_dir, culture_type, media, feature='od', plastic=False, members=None, 
                   rm_compet_excl=False, ce_feature='od', min_survived=1.0e-2, method='mean'):
    
    if (method != 'mean') & (method != 'std'):
        raise ValueError('Value for `method` should only be "mean" or "std".')
    
    for i in range(len(media)):
        layer, layer_meta = stack_co_replicates(data_dir, culture_type, media[i], feature=feature, plastic=plastic, members=members)
        
        if i == 0:
            data_mat = np.zeros((len(media), len(layer_meta['species_ID']), len(layer_meta['species_ID'])))
            
        if rm_compet_excl:
            ce_stack, ce_meta = competitive_exclusion_stack(data_dir, culture_type, media[i], 
                                                            feature=ce_feature, min_survived=min_survived,
                                                            plastic=plastic, members=members)
            layer[ce_stack != 0.0] = np.nan
        
        if method == 'mean':
            data_mat[i, :, :] = np.nanmean(layer, axis=0) # averaging across replicates for each medium
        else:
            data_mat[i, :, :] = np.nanstd(layer, axis=0) # taking standard deviation across replicates for each medium
    
    layer_meta['media'] = media
    
    return data_mat, layer_meta

def stack_mono_replicates(data_dir, culture_type, media=None, feature='od', plastic=False, species_ID_list=None):
    # feature = 'od' or 'p_alive' or 'init_r'
    mode = 'plastic' if plastic else 'sfm'
    
    if feature == 'init_r':
        data_file = data_dir + 'init_r.' + culture_type + '_mono-culture.tsv'
    else:
        data_file = data_dir + 'final_point.' + culture_type + '_mono-culture.' + feature + '.tsv'
    
    essen = ['rep_name', 'species_ID', 'mode']
    cols = None if media is None else essen + media
    df = pd.read_csv(data_file, sep='\t', usecols=cols)
    if cols is not None: # this is important, as to make sure the output data matrix would have a same order of medium as provided in `media`
        df = df[cols]
    
    species_ID_list = species_ID_list if species_ID_list is not None else df['species_ID'].unique()
    
    data = df.loc[(df['mode'] == mode) & (df['species_ID'].isin(species_ID_list)), :]
    
    replicates = data['rep_name'].unique()

    n_replicates = len(replicates)
    n_media = data.shape[1] - len(essen)
    n_species = len(species_ID_list)
    
    data_mat = np.zeros((n_replicates, n_species, n_media))
    for i in range(n_replicates):
        data_layer = data.loc[data['rep_name'] == replicates[i], :]
        data_layer = data_layer.set_index('species_ID', drop=False)
        data_layer = data_layer.reindex(species_ID_list) # this is important, as to make sure the output data matrix would have a same order of species as provided in `species_ID_list`
        
        layer = data_layer.drop(essen, axis=1).to_numpy(dtype='float')
        data_mat[i, :, :] = layer

    return data_mat, species_ID_list

def interaction_coeffs(co_data_dir, mono_data_dir, culture_type, media, plastic=False, members=None, 
                      rm_compet_excl=True, ce_feature='od', min_survived=1.0e-2,
                      intra=False, method='glv'):
    
    co_mat, co_meta = stack_co_media(co_data_dir, culture_type, media, 
                                     feature='od', plastic=plastic, members=members,
                                     rm_compet_excl=rm_compet_excl, ce_feature=ce_feature, min_survived=min_survived)
    
    mono_stack, mono_meta = stack_mono_replicates(mono_data_dir, culture_type, media, feature='od', plastic=plastic,
                                                  species_ID_list=co_meta['species_ID'])
    mono_mat = mono_stack.mean(axis=0) # averaging across replicates
    
    if method == 'glv': # compute accordingly to the generalized Lotka-Volterra (gLV) model, else modified gLV will be followed
        # extract intrinsic growth rate. Intrinsic growth rate = initial growth rate in our simulation, when the species populations are still tiny and so, there is no pressure of either intra- or inter-species resource competition
        inr_stack, mono_meta = stack_mono_replicates(mono_data_dir, culture_type, media, feature='init_r', plastic=plastic,
                                                     species_ID_list=co_meta['species_ID'])
        inr_mat = inr_stack.mean(axis=0) # averaging across replicates
    
    data_mat = np.zeros(co_mat.shape)
    
    for k in range(len(media)):
        for i in range(len(co_meta['species_ID'])):
            for j in range(len(co_meta['species_ID'])):
                if i == j:
                    if intra:
                        data_mat[k, i, j] = -1/mono_mat[i, k]*inr_mat[i, k] if method == 'glv' else -1/mono_mat[i, k]
                    else:
                        data_mat[k, i, j] = np.nan
                elif method == 'glv':
                    data_mat[k, i, j] = np.nan if np.isnan(co_mat[k,i,j]) else (co_mat[k, i, j] / mono_mat[i, k] - 1) * inr_mat[i, k] / co_mat[k, j, i]  
                else:
                    data_mat[k, i, j] = np.nan if np.isnan(co_mat[k,i,j]) else (co_mat[k, i, j] / mono_mat[i, k] - 1) / co_mat[k, j, i]
    
    return data_mat, co_meta

def linear_stability(co_data_dir, mono_data_dir, culture_type, media, plastic=False, members=None, 
                     rm_compet_excl=True, ce_feature='od', min_survived=1.0e-2):
    
    densities_stack_media, co_meta = stack_co_media(co_data_dir, culture_type, media, 
                                                    feature='od', plastic=plastic, members=members,
                                                    rm_compet_excl=rm_compet_excl, ce_feature=ce_feature, min_survived=min_survived)
    
    interactions_stack_media, co_meta = interaction_coeffs(co_data_dir, mono_data_dir, culture_type, media, plastic=plastic,
                                                           rm_compet_excl=rm_compet_excl, ce_feature=ce_feature, min_survived=min_survived,
                                                           intra=True, method='glv')
    
    output_matrix = np.full(interactions_stack_media.shape, np.nan)

    for m in range(len(media)):
        interactions_mat = interactions_stack_media[m, :, :]
        co_density_mat = densities_stack_media[m, :, :]

        for i in range(interactions_mat.shape[0]-1):
            s = i+1
            for j in range(s, interactions_mat.shape[0]):

                if i != j:
                    
                    # extract community matrix A of between 2 species i & j
                    ixgrid = np.ix_([i, j], [i, j])
                    A = interactions_mat[ixgrid]
                    
                    if np.isnan(A).sum() == 0.0: # if none of the interaction coefficients is NaN, procede linear stability analysis
                        
                        # derive Jacobian matrix from community matrix & vector of species densities at equilibrium of co-culture
                        J = np.zeros(A.shape)
                        J[0, :] = A[0, :] * co_density_mat[i, j]
                        J[1, :] = A[1, :] * co_density_mat[j, i]

                        eigvals = linalg.eigvals(J) # eigenvalues
                        max_re_eigval = np.max(eigvals.real) # max real part of all eigenvalues

                        output_matrix[m, j, i] = max_re_eigval
                    else:
                        output_matrix[m, j, i] = np.nan
                    
    return output_matrix, interactions_stack_media, densities_stack_media, co_meta