import pandas as pd
import numpy as np

import gc # Garbage Collector for releasing unreferenced memory

from my_modules import compute

from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.collections
import seaborn as sns

sns.set_style("whitegrid")

font = {'family': 'sans-serif', 'serif' : 'Helvetica',
        'size'   : 25}
plt.rc('font', **font)

my_quali_colormaps = ['colorblind', 'Dark2']
final_OD_palette = {'ON': '#ffb219', 'OFF': '#a1a9c5'}

#################################################
# Plotting bacteria density (OD) over time

def plot_OD_over_time(storage, simID, replicates, media, save_fig=True, fig_dir="./", fig_name=None):

    # Monitor running process & validate arguments
    status = 0
    
    check_culture_type = set(r.rsplit('_')[1] for r in replicates)

    if len(check_culture_type) > 1:
        raise ValueError('Function only works for one type of culture, either batch or continuous')

    # Metadata of species names & ecotypes
    meta_file = storage + 'selected_species_setA.tsv'
    meta = pd.read_csv(meta_file, sep="\t")
    
    # Prepare dataframe
    merged_data = pd.DataFrame()

    for r in replicates:
        rep = r.rsplit('_')[0]

        for m in media:
            data_dir = storage + rep + '/v2.10.0_sim_' + simID + '/experiments/' + list(check_culture_type)[0] + '_coculture/'  + m + '/'
            data_file = data_dir + 'species_trajectory.tsv'

            df = pd.read_csv(data_file, sep="\t", usecols=['species_name', 'time', 'od'])
            df['media'] = m
            df['replicate'] = rep

            merged_data = pd.concat([merged_data, df])

    members = meta.loc[meta['Species_name'].isin(df['species_name'].unique()), :]
#     print(members)

    merged_data.reset_index(drop=True)

    # Plotting
    fig = plt.figure(figsize=(14, 15*len(media)))

    outer = gridspec.GridSpec(nrows=len(media), ncols=1, wspace=0.2, hspace=0.2)

    for i in range(len(media)):
        inner = gridspec.GridSpecFromSubplotSpec(nrows=members.shape[0], ncols=1,
                                                 subplot_spec=outer[i], wspace=0.15, hspace=0.15)

        for j in range(members.shape[0]):

            plot_data = merged_data.loc[(merged_data['species_name'] == members.iloc[j, 3]) & (merged_data['media'] == media[i]), 
                                        ['time', 'od', 'replicate']]

            plot_ax = plt.Subplot(fig, inner[j])

            kwargs={'linewidth':3}
            g = sns.lineplot(data = plot_data, x='time', y='od', hue='replicate', ax=plot_ax, 
                             palette = sns.color_palette(my_quali_colormaps[1], len(replicates)), 
                             **kwargs)
            title = members.iloc[j, 1] + " (" + members.iloc[j, 2] + ")" 
            plot_ax.set(title=title, ylabel='OD')

            if j == members.shape[0]-1:
                plot_ax.set(xlabel='Time (h)')
            else:
                plot_ax.set(xlabel='', xticklabels=[])

            if j == 0:

                plot_ax.text(0.0, 1.1, s=media[i], ha='center', va='center', transform=plot_ax.transAxes, 
                             fontweight='bold')
                lgnd = plot_ax.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0), title='Replicate')
                # Change width of lines in legend
                for line in lgnd.get_lines():
                    line.set_linewidth(5)

            else:
                plot_ax.get_legend().remove()

            fig.add_subplot(plot_ax)
    
    # Save figure
    if save_fig:
        fig_name = fig_dir + simID + "." + list(check_culture_type)[0] + ".OD_over_time" + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close("all")
        plt.clf()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
        
    # Freeing memory usage
    del df, merged_data, plot_data
    del fig
    gc.collect()
    
    return status

#################################################
# Plotting distribution of final OD across replicates

def plot_OD_dist(plastic, sfm, palette, ax):
    if plastic.std() > 0.1:
        sns.violinplot(data=plastic, color=palette['ON'], saturation=1.0, ax=ax)
        ax.collections[0].set_alpha(0.3)

    ax.axhline(y=plastic.mean(), color=palette['ON'], linewidth=4.0)

    sns.swarmplot(data=plastic, color=palette['ON'], edgecolor="white", size=10, ax=ax)

    ax.axhline(y=sfm, color=palette['OFF'], linewidth=4.0)

    ax.set_xticklabels('')
    ax.yaxis.set_major_formatter(plt.FormatStrFormatter('%.2f'))
    return ax

def plot_OD_dist_community(data_dir, culture_type, medium, feature='od', members=None, save_fig=True, fig_dir="./", fig_name=None):
    
    # Monitor running process & validate arguments
    status = 0
    
    if culture_type not in ['batch', 'continuous']:
        raise ValueError('Name of culture type should only be either \'batch\' or \'continuous\'')
    
    # Prepare data
    plastic_mat, plastic_meta = compute.stack_co_replicates(data_dir, culture_type, medium, feature=feature, plastic=True, members=members)
    sfm_mat, sfm_meta = compute.stack_co_replicates(data_dir, culture_type, medium, feature=feature, plastic=False, members=members)
    
    members = plastic_meta['ecotype_ID']
    species_IDs = plastic_meta['species_ID']
            
    n_members = len(members)
    
    # Plotting
    fig = plt.figure(figsize=(10+5*n_members, 6*n_members))
    fig.suptitle(culture_type.capitalize() + ' culture | ' + medium, y=0.92, fontsize='x-large')
    
    outer = gridspec.GridSpec(nrows=1, ncols=2, figure=fig, width_ratios=(0.01, 0.99))
    
    main_plots = gridspec.GridSpecFromSubplotSpec(nrows=n_members, ncols=n_members, subplot_spec=outer[1], wspace=0.5, hspace=0.2)
    species_col = gridspec.GridSpecFromSubplotSpec(nrows=n_members, ncols=1, subplot_spec=outer[0], wspace=0.0, hspace=0.2)
    
    for i in range(n_members):
        sp_A = members[i]
        species_ax = fig.add_subplot(species_col[i])
        species_ax.text(x=0.5, y=0.5, s=species_IDs[i], rotation=90, ha='center', va='center', fontsize='large')
        species_ax.axis('off')
        
        for j in range(n_members):
            if (i != j):
                sp_B = members[j]
                plastic = plastic_mat[:, i, j]
                sfm = sfm_mat[0, i, j]

                plot_ax = fig.add_subplot(main_plots[i, j])

                plot_ax = plot_OD_dist(plastic, sfm, final_OD_palette, plot_ax)
                
                if i==n_members-1:
                    plot_ax.set_xlabel(species_IDs[j], fontsize='large')
                
                plot_ax.axis('tight')
            
            elif i==n_members-1:
                plot_ax = fig.add_subplot(main_plots[i, j])
                plot_ax.text(x=0.5, y=-0.1, s=species_IDs[j], ha='center', va='center', fontsize='large')
                plot_ax.axis('off')
        
    # Save figure
    if save_fig:
        fig_name = fig_dir + "OD_dist." + culture_type + '_' + medium + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close("all")
        plt.clf()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
    
    return status

def plot_interaction_OD_dist(co_data_dir, mono_data_dir, culture_type, medium, feature='od', members=None, save_fig=True, fig_dir="./", fig_name=None):
    
    # Monitor running process & validate arguments
    status = 0
    
    if culture_type not in ['batch', 'continuous']:
        raise ValueError('Name of culture type should only be either \'batch\' or \'continuous\'')
    
    # Prepare data
    
    ## plastic = True
    co_stack, co_meta = compute.stack_co_replicates(co_data_dir, culture_type, medium, feature=feature, plastic=True, members=members)
    mono_stack, mono_meta = compute.stack_mono_replicates(mono_data_dir, culture_type, [medium], feature=feature, plastic=True, species_ID_list=co_meta['species_ID'])
    
    members = co_meta['ecotype_ID']
    species_IDs = co_meta['species_ID']
            
    n_members = len(members)
    
    ## plastic = False 
    sfm_co_stack, sfm_co_meta = compute.stack_co_replicates(co_data_dir, culture_type, medium, feature=feature, plastic=False, members=co_meta['ecotype_ID'])
    sfm_mono_stack, sfm_mono_meta = compute.stack_mono_replicates(mono_data_dir, culture_type, [medium], feature=feature, plastic=False, 
                                                                  species_ID_list=co_meta['species_ID'])
    
    
    # Plotting
    fig = plt.figure(figsize=(10+7*n_members, 6*n_members))
    fig.suptitle(culture_type.capitalize() + ' culture | ' + medium, y=0.92, fontsize='x-large')
    
    outer = gridspec.GridSpec(nrows=1, ncols=2, figure=fig, width_ratios=(0.005, 0.995))
    
    main_plots = gridspec.GridSpecFromSubplotSpec(nrows=n_members, ncols=n_members, subplot_spec=outer[1], wspace=0.4, hspace=0.2)
    species_col = gridspec.GridSpecFromSubplotSpec(nrows=n_members, ncols=1, subplot_spec=outer[0], wspace=0.0, hspace=0.2)
    
    for i in range(n_members):
        sp_A = members[i]
        species_ax = fig.add_subplot(species_col[i])
        species_ax.text(x=0.5, y=0.5, s=species_IDs[i], rotation=90, ha='center', va='center', fontsize='large')
        species_ax.axis('off')
        
        for j in range(n_members):
            if (i != j):
                sp_B = members[j]
                
                ## plastic = True
                co = co_stack[:, i, j]
                mono = mono_stack[:, i, 0]
                ## plastic = False
                sfm_co = sfm_co_stack[:, i, j]
                sfm_mono = sfm_mono_stack[:, i, 0]
    
                plot_ax = fig.add_subplot(main_plots[i, j])
    
                sns.violinplot(data=[mono, co], scale='width', palette=['#a47bb2', '#9abca7'], saturation=1.0, ax=plot_ax)
                for colle in plot_ax.collections:
                    if isinstance(colle, matplotlib.collections.PolyCollection):
                        colle.set_alpha(0.3)
                
                ## mean for plastic = True
                plot_ax.axhline(y=mono.mean(), xmin=0.05, xmax=0.45, color='#a47bb2', linewidth=3.0)
                plot_ax.axhline(y=co.mean(), xmin=0.55, xmax=0.95, color='#9abca7', linewidth=3.0)
                ## mean for plastic = False
                plot_ax.axhline(y=sfm_mono.mean(), xmin=0.05, xmax=0.45, color='#a47bb2', linewidth=3.0, linestyle='--')
                plot_ax.axhline(y=sfm_co.mean(), xmin=0.55, xmax=0.95, color='#9abca7', linewidth=3.0, linestyle='--')
                    
                sns.swarmplot(data=[mono, co], palette=['#a47bb2', '#9abca7'], edgecolor="white", size=10, ax=plot_ax)
                
                plot_ax.set_xticklabels('')
                
                if i==n_members-1:
                    plot_ax.set_xlabel(species_IDs[j], fontsize='large')
                
                plot_ax.axis('tight')
            
            elif i==n_members-1:
                plot_ax = fig.add_subplot(main_plots[i, j])
                plot_ax.text(x=0.5, y=-0.1, s=species_IDs[j], ha='center', va='center', fontsize='large')
                plot_ax.axis('off')
        
    # Save figure
    if save_fig:
        fig_name = fig_dir + "interaction_OD_dist." + culture_type + '_' + medium + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close("all")
        plt.clf()
    
        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
    
    return status