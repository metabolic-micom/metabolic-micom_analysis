import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from scipy.spatial import distance

def flux_dynamics(storage, replicate, speciesID, culture_type, media, tmax=None, normalize=True, plot_biomass=True, return_traveled_distance=False):
    simID = speciesID + '_plastic'
    
    data_dir = storage + replicate + '/v2.10.0_sim_' + simID + '/experiments/' + culture_type + '_mono-culture/'  + media + '/'
    data_file = data_dir + 'species_avg_fluxes.tsv'

    df = pd.read_csv(data_file, sep="\t")
    
    if plot_biomass:
        plt.plot(df['time'], df['avg_bm'])
        plt.title('Average biomass production rate')
        
    output = []
    traveled_dist = 0.0
    
    if tmax is None:
        cutoff = df.shape[0] # use all data
    else:
        cutoff = df.loc[df['time'] <= tmax, :].shape[0] # use data with time <= tmax
    
    for i in range(cutoff-1):
        t_start = df.iloc[i, 0]
        t_end = df.iloc[i+1, 0]

        if (t_end == t_start): # repeated data sampled because of doppriStepper
            continue # skip and move to the next time point, which is the repeated one
        else:
            bm_start = df.iloc[i, 2]
            vr_start = np.array(df.iloc[i, 4:], dtype='float') 

            bm_end = df.iloc[i+1, 2]
            vr_end = np.array(df.iloc[i+1, 4:], dtype='float')

            # Normalize flux rates by average biomass production rate if there are some biomass production
            # else there is case with biomass production = 0.0 and all average flux rates = 0.0
            if normalize:
                vr_start_norm = vr_start / bm_start if bm_start > 0.0 else vr_start 
                vr_end_norm = vr_end / bm_end if bm_end > 0.0 else vr_end

                euclidean_dist = distance.euclidean(vr_end_norm, vr_start_norm)
            else:
                euclidean_dist = distance.euclidean(vr_end, vr_start)
            
            if return_traveled_distance:
                traveled_dist += euclidean_dist
            else:
                rate_of_change = euclidean_dist / (t_end - t_start)
                output.append([t_start, t_end, euclidean_dist, rate_of_change])
    
    if return_traveled_distance:
        return traveled_dist
    else:
        fl_dyn = pd.DataFrame(output, columns=['t_start', 't_end', 'euclidean_dist', 'rate_of_change'])
        return fl_dyn