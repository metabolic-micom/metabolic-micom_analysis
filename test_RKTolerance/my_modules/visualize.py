import pandas as pd
import numpy as np

import os
import warnings

import gc # Garbage Collector for releasing unreferenced memory

# from my_modules import compute

from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns

sns.set_style("whitegrid")

font = {'family': 'sans-serif', 'serif' : 'Helvetica',
        'size'   : 25}
plt.rc('font', **font)

my_quali_colormaps = ['colorblind', 'Dark2']

#################################################
# Plotting bacteria density (OD) over time

def plot_OD_over_time(storage, communityID, cases, media, save_fig=True, fig_dir="./", fig_name=None):

    # Monitor running process & validate arguments
    status = 0
    
    check_culture_type = set(c.rsplit('_')[1] for c in cases)

    if len(check_culture_type) > 1:
        raise ValueError('Function only works for one type of culture, either batch or continuous')

    # Metadata of species names & ecotypes
    meta_file = storage + 'selected_species_setA.tsv'
    meta = pd.read_csv(meta_file, sep="\t")
    
    # Simulation ID = community ID in PLASTIC mode
    simID = communityID + '_plastic'
    
    # Number of cases having data for each media
    cases_by_media = {x: [] for x in media} # start with dictionary of keys as media & values as empty lists
    
    # Prepare dataframe
    merged_data = pd.DataFrame()

    for c in cases:
        case = c.rsplit('_')[0]

        for m in media:
            data_dir = storage + '/v2.10.0_sim_' + simID + '.' + case + '/experiments/' + list(check_culture_type)[0] + '_coculture/'  + m + '/'
            data_file = data_dir + 'species_trajectory.tsv'
            
            if os.path.isfile(data_file):
                
                cases_by_media[m].append(case)
                
                df = pd.read_csv(data_file, sep="\t", usecols=['species_name', 'time', 'od'])
                df['media'] = m
                df['RKTolerance'] = '1.0E-' + case.replace('RKTo','')

                merged_data = pd.concat([merged_data, df])
                
            else:
                mess = ['There is no output for simulation in', m, '+', list(check_culture_type)[0], 'culture for case', case]
                warnings.warn(" ".join(mess))
                
                

    members = meta.loc[meta['Species_name'].isin(df['species_name'].unique()), :]
#     print(members)

    merged_data.reset_index(drop=True)

    # Plotting
    fig = plt.figure(figsize=(14, 15*len(media)))

    outer = gridspec.GridSpec(nrows=len(media), ncols=1, wspace=0.0, hspace=0.2)

    for i in range(len(media)):
        inner = gridspec.GridSpecFromSubplotSpec(nrows=members.shape[0], ncols=1,
                                                 subplot_spec=outer[i], wspace=0.0, hspace=0.15)
        
        n_cases = len(list(cases_by_media.values())[i]) # number of cases having data for the targeted media
        
        if n_cases < 4:
            pal = sns.color_palette(my_quali_colormaps[1], n_cases)
        else:
            stupid_pal = sns.color_palette(my_quali_colormaps[1], n_cases)
            pal = [stupid_pal[0], stupid_pal[1], stupid_pal[3], stupid_pal[2]]

        for j in range(members.shape[0]):

            plot_data = merged_data.loc[(merged_data['species_name'] == members.iloc[j, 3]) & (merged_data['media'] == media[i]), 
                                        ['time', 'od', 'RKTolerance']]

            plot_ax = plt.Subplot(fig, inner[j])

            kwargs={'linewidth':3}
            g = sns.lineplot(data = plot_data, x='time', y='od', hue='RKTolerance', ax=plot_ax, 
                             palette = pal, hue_order = plot_data['RKTolerance'].unique(),
                             **kwargs)
            title = members.iloc[j, 1] + " (" + members.iloc[j, 2] + ")" 
            plot_ax.set(title=title, ylabel='OD')

            if j == members.shape[0]-1:
                plot_ax.set(xlabel='Time (h)')
            else:
                plot_ax.set(xlabel='', xticklabels=[])

            if j == 0:

                plot_ax.text(0.0, 1.1, s=media[i], ha='center', va='center', transform=plot_ax.transAxes, 
                             fontweight='bold')
                lgnd = plot_ax.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0), title='RKTolerance')
                # Change width of lines in legend
                for line in lgnd.get_lines():
                    line.set_linewidth(5)

            else:
                plot_ax.get_legend().remove()

            fig.add_subplot(plot_ax)
    
    # Save figure
    if save_fig:
        fig_name = fig_dir + simID + "." + list(check_culture_type)[0] + ".OD_over_time" + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close("all")
        plt.clf()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
        
    # Freeing memory usage
    del df, merged_data, plot_data
    del fig
    gc.collect()
    
    return status

#################################################
# Plotting bacteria growth rate over time or over density (OD)

def plot_growthRate_over_time(storage, communityID, cases, media, save_fig=True, fig_dir="./", fig_name=None):

    # Monitor running process & validate arguments
    status = 0
    
    check_culture_type = set(c.rsplit('_')[1] for c in cases)

    if len(check_culture_type) > 1:
        raise ValueError('Function only works for one type of culture, either batch or continuous')
    
    #### Get hardcoded parameters
    hardcoded_param = pd.read_csv(storage + 'hardcoded_parameters.tsv', sep='\t')
    flowRate = hardcoded_param['flowRate'][0]
    fluxNGAM = hardcoded_param['fluxNGAM'][0]

    # Metadata of species names & ecotypes
    meta_file = storage + 'selected_species_setA.tsv'
    meta = pd.read_csv(meta_file, sep="\t")
    
    # Simulation ID = community ID in PLASTIC mode
    simID = communityID + '_plastic'
    
    # Number of cases having data for each media
    cases_by_media = {x: [] for x in media} # start with dictionary of keys as media & values as empty lists
    
    # Prepare dataframe
    merged_data = pd.DataFrame()

    for c in cases:
        case = c.rsplit('_')[0]

        for m in media:
            data_dir = storage + '/v2.10.0_sim_' + simID + '.' + case + '/experiments/' + list(check_culture_type)[0] + '_coculture/'  + m + '/'
            data_file = data_dir + 'species_trajectory.tsv'

            if os.path.isfile(data_file):
                
                cases_by_media[m].append(case)
                
                df = pd.read_csv(data_file, sep="\t", usecols=['species_name', 'time', 'r'])
                df['media'] = m
                df['RKTolerance'] = '1.0E-' + case.replace('RKTo','')

                merged_data = pd.concat([merged_data, df])
                
            else:
                mess = ['There is no output for simulation in', m, '+', list(check_culture_type)[0], 'culture for case', case]
                warnings.warn(" ".join(mess))

    members = meta.loc[meta['Species_name'].isin(df['species_name'].unique()), :]
#     print(members)

    merged_data.reset_index(drop=True)

    # Plotting
    fig = plt.figure(figsize=(14, 15*len(media)))

    outer = gridspec.GridSpec(nrows=len(media), ncols=1, wspace=0.0, hspace=0.2)

    for i in range(len(media)):
        inner = gridspec.GridSpecFromSubplotSpec(nrows=members.shape[0], ncols=1,
                                                 subplot_spec=outer[i], wspace=0.0, hspace=0.15)
        n_cases = len(list(cases_by_media.values())[i]) # number of cases having data for the targeted media
        
        if n_cases < 4:
            pal = sns.color_palette(my_quali_colormaps[1], n_cases)
        else:
            stupid_pal = sns.color_palette(my_quali_colormaps[1], n_cases)
            pal = [stupid_pal[0], stupid_pal[1], stupid_pal[3], stupid_pal[2]]
        
        for j in range(members.shape[0]):

            plot_data = merged_data.loc[(merged_data['species_name'] == members.iloc[j, 3]) & (merged_data['media'] == media[i]), 
                                        ['time', 'r', 'RKTolerance']]

            plot_ax = plt.Subplot(fig, inner[j])

            kwargs={'linewidth':3}
            g = sns.lineplot(data = plot_data, x='time', y='r', hue='RKTolerance', ax=plot_ax, 
                             palette = pal, hue_order = plot_data['RKTolerance'].unique(), 
                             **kwargs)
            
            # Add constant lines
            plot_ax.axhline(y=0.0, color='black', linestyle='--')
            plot_ax.axhline(y=flowRate, color='blue', linestyle='--')
            plot_ax.axhline(y=-fluxNGAM, color='red', linestyle='--') 
            
            # Polish figure
            title = members.iloc[j, 1] + " (" + members.iloc[j, 2] + ")" 
            plot_ax.set(title=title, ylabel='Growth rate')

            if j == members.shape[0]-1:
                plot_ax.set(xlabel='Time (h)')
            else:
                plot_ax.set(xlabel='', xticklabels=[])

            if j == 0:

                plot_ax.text(0.0, 1.1, s=media[i], ha='center', va='center', transform=plot_ax.transAxes, 
                             fontweight='bold')
                lgnd = plot_ax.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0), title='RKTolerance')
                # Change width of lines in legend
                for line in lgnd.get_lines():
                    line.set_linewidth(5)

            else:
                plot_ax.get_legend().remove()

            fig.add_subplot(plot_ax)
    
    # Save figure
    if save_fig:
        fig_name = fig_dir + simID + "." + list(check_culture_type)[0] + ".growthRate_over_time" + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close("all")
        plt.clf()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
        
    # Freeing memory usage
    del df, merged_data, plot_data
    del fig
    gc.collect()
    
    return status

#################################################
# Plotting dynamics of metabolites in the environment

def plot_env_metabolites(storage, communityID, cases, media, save_fig=True, fig_dir="./", fig_name=None):

    # Monitor running process & validate arguments
    status = 0
    
    check_culture_type = set(c.rsplit('_')[1] for c in cases)

    if len(check_culture_type) > 1:
        raise ValueError('Function only works for one type of culture, either batch or continuous')

    # Simulation ID = community ID in PLASTIC mode
    simID = communityID + '_plastic'

    # Figure object & outer layer
    fig = plt.figure(figsize=(8*len(cases), 10*len(media)))

    outer = gridspec.GridSpec(nrows=len(media), ncols=1, wspace=0.0, hspace=0.3)

    # Create data frame & plot for each media:
    for i in range(len(media)):
        m = media[i]
        avail_cases = [] # list of cases having data for the targeted media

        # Prepare dataframe
        merged_data = pd.DataFrame()

        for c in cases:
            case = c.rsplit('_')[0]

            data_dir = storage + '/v2.10.0_sim_' + simID + '.' + case + '/experiments/' + list(check_culture_type)[0] + '_coculture/'  + m + '/'
            data_file = data_dir + 'metabolites_trajectory.tsv'

            if os.path.isfile(data_file):

                df = pd.read_csv(data_file, sep="\t")
                metabolites = df.columns[1:]

                case_label = '1.0E-' + case.replace('RKTo','')
                df['RKTolerance'] = case_label
                avail_cases.append(case_label)

                merged_data = pd.concat([merged_data, df])

            else:
                mess = ['There is no output for simulation in', m, '+', list(check_culture_type)[0], 'culture for case', case]
                warnings.warn(" ".join(mess))

        unchanged_metabolites = list(metabolites[merged_data[metabolites].sum() == 0.0])
        metabolites = list(metabolites[merged_data[metabolites].sum() != 0.0])

        merged_data.drop(labels = unchanged_metabolites, axis=1, inplace=True)

        # Plotting
        inner = gridspec.GridSpecFromSubplotSpec(nrows=1, ncols=len(avail_cases),
                                                 subplot_spec=outer[i], wspace=0.15, hspace=0.15)

        for j in range(len(avail_cases)):

            plot_data = merged_data.loc[merged_data['RKTolerance'] == avail_cases[j], :]
            plot_data = pd.melt(plot_data, id_vars='time', value_vars=metabolites, var_name = 'metabolite', value_name='concentration')

            plot_ax = plt.Subplot(fig, inner[j])

            kwargs={'linewidth':1}
            g = sns.lineplot(data = plot_data, x='time', y='concentration', hue='metabolite', ax=plot_ax, legend=False,
                             **kwargs)
            title = 'RKTolerance = ' + avail_cases[j]
            plot_ax.set(title=title, xlabel='Time (h)')

            if j == 0:
                plot_ax.text(0.0, 1.1, s=media[i], ha='center', va='center', transform=plot_ax.transAxes, 
                             fontweight='bold')
                plot_ax.set(ylabel='Concentration (mM)')
            else:
                plot_ax.set(ylabel='')

            fig.add_subplot(plot_ax)

    # Save figure
    if save_fig:
        fig_name = fig_dir + simID + "." + list(check_culture_type)[0] + ".env_metabolites" + ".svg" if fig_name is None else fig_name
        plt.savefig(fig_name, transparent=True, bbox_inches='tight')
        plt.close("all")
        plt.clf()

        status = fig_name + ": Plotted & Saved."
    else:
        status = 1
        
    # Freeing memory usage
    del df, merged_data, plot_data
    del fig
    gc.collect()
    
    return status