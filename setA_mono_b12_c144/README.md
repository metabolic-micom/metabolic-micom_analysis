# Analyses on mono-culture simulation outputs

## Order of running

### Population density & growth rate
1. Prepare directory `fig/OD_over_time/` --> `OD_over_time.ipynb`
2. Prepare directory `fig/final_OD/` --> `final_OD.ipynb`
3. Prepare directory `fig/std_final_OD/` --> `std_final_OD.ipynb`

### Effect of plasticity $P_{mono}$
4. `classify_media.ipynb`
5. `compute.log_fold_change.plasticity.ipynb` --> `log_fold_change.plasticity.ipynb`
6. `compute.log_fold_change.culture_type.ipynb` --> `log_fold_change.culture_type.ipynb`

### Mechanistic understanding
7. `metaprod_dynamics.ipynb` 
8. `flux_dynamics.ipynb` 
9. `compute.metabolic_travel_distance.ipynb` --> `metabolic_travel_distance.ipynb` 
10. `initr.ipynb`